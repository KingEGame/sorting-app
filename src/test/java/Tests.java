import com.epam.rd.autotasks.Main;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Tests {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testNotNumberCase(){
        provideInput("abracadabra ada ");
        Main.sort();
        assertThat(outContent.toString(), is("not number"));
    }

    @Test
    public void testEmptyCase(){
        provideInput("1 2 3 4 5 6 9 8 7 10");
        Main.sort();
        assertThat(outContent.toString(), is("1 2 3 4 5 6 7 8 9 10 "));
    }

    @Test
    public void testSingleCase(){
        provideInput("1 ");
        Main.sort();
        assertThat(outContent.toString(), is("1 "));
    }

    @Test
    public void testSortedCase(){
        provideInput("1 2 3 4 5 6 9 8 7 10");
        Main.sort();
        assertThat(outContent.toString(), is("1 2 3 4 5 6 7 8 9 10 "));
    }


    @Test
    public void testBigSortedCase(){
        provideInput("438463 267475 153437 -552373 -583134 -709034 -182102 340166 804667 319268");
        Main.sort();
        assertThat(outContent.toString(), is("-709034 -583134 -552373 -182102 153437 267475 319268 340166 438463 804667 "));
    }

    void provideInput(String data) {
        ByteArrayInputStream testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

}
